#include<stdio.h>
typedef struct fract
{
	int num,deno;
}fract;
fract add(fract,fract);
int gcd(int a,int b)
{
	int gcd,rem;
	while(a!=0)
	{
		rem=b%a;
		b=a;
		a=rem;
	}
	gcd=b;
	return gcd;
}

void input(fract *f1,fract *f2)
{
	scanf("%d/%d",&f1->num,&f1->deno);
	scanf("%d/%d",&f2->num,&f2->deno);
}
struct fract add(struct fract f1,struct fract f2)
{
	fract result={(f1.num * f2.deno) + (f2.num * f1.deno), f1.deno * f2.deno};
	int g=gcd(result.num,result.deno);
	result.num=result.num/g;
	result.deno=result.deno/g;
	return result;

}
void output(fract f1,fract f2,fract result)	
{
	printf("The sum of %d/%d and %d/%d is %d/%d",f1.num,f1.deno,f2.num,f2.deno,result.num,result.deno);
}
int main()
{
	
	fract f1,f2,a;
	input(&f1,&f2);
	a=add(f1,f2);
	output(f1,f2,a);
	return 0;
}
