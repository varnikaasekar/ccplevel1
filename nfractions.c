#include<stdio.h>
int gcd(int a,int b)
{
	if (b == 0) 
	{
        return a;
    }
	int g=gcd(b, a % b);
	return g;
}
int lcm(int d[],int t)
{
	int ans=d[0];
	for(int i=0;i<t;i++)
		ans=(((d[i]*ans))/(gcd(d[i],ans)));
	return ans;
}
void output(int t,int n[],int d[],int an,int ad)
{
	for(int i=0;i<t-1;i++)
		printf("%d/%d+",n[i],d[i]);
	printf("%d/%d=%d/%d",n[t-1],d[t-1],an,ad);
}
void reducedfraction(int n[],int d[],int t)
{
	int ad,an=0;
	ad=lcm(d,t);
	for(int i=0;i<t;i++)
	{
		an=an+(n[i])*(ad/d[i]);
	}
	int g=gcd(an,ad);
	an=an/g;
	ad=ad/g;
	output(t,n,d,an,ad);
}
void input(int n[],int d[],int *t)
{
	scanf("%d",t);
	for(int i=0;i<*t;i++)
		scanf("%d",&n[i]);
	for(int i=0;i<*t;i++)
		scanf("%d",&d[i]);
}

int main()
{
	
int n[100],d[100],t;
input(n,d,&t);
reducedfraction(n,d,t);

return 0;
}
