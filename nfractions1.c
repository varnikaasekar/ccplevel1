#include<stdio.h>
typedef struct fractions
{
	int num,deno;
	
}fract;
int gcd(int a,int b)
{
	if (b == 0) 
	{
        return a;
    }
	int g=gcd(b, a % b);
	return g;
}
int lcm(struct fractions n[],int t)
{
	int ans=n[0].deno;
	for(int i=0;i<t;i++)
		ans=(((n[i].deno*ans))/(gcd(n[i].deno,ans)));
	return ans;
};
void output(int t,fract result,struct fractions n[])
{
	for(int i=0;i<t-1;i++)
		printf("%d/%d+",n[i].num,n[i].deno);
	printf("%d/%d=%d/%d",n[t-1].num,n[t-1].deno,result.num,result.deno);
}
fract reducedfraction(struct fractions n[],int t)
{
	fract result;
	result.num=0;
	result.deno=lcm(n,t);
	for(int i=0;i<t;i++)
	{
		result.num=result.num+(n[i].num)*(result.deno/n[i].deno);
	}
	int g=gcd(result.num,result.deno);
	result.num=result.num/g;
	result.deno=result.deno/g;
	return result;
}
void input(struct fractions n[],int *t)
{
	scanf("%d",t);
	for(int i=0;i<*t;i++)
		scanf("%d",&n[i].num);
	for(int i=0;i<*t;i++)
		scanf("%d",&n[i].deno);
}

int main()
{
	fract a;
	struct fractions n[100];
    int t;
    input(n,&t);
    a=reducedfraction(n,t);
    output(t,a,n);
    return 0;
}
