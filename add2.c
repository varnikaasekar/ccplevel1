#include<stdio.h>
int input()
{
        int a;
        scanf("%d",&a);
        return a;

}
int add(int *a,int *b)
{
        int sum;
        sum=*a+*b;
        return sum;
}
void output(int *a,int *b,int *c)
{
        printf("the sum of %d and %d is %d\n",*a,*b,*c);
}
int main()
{
        int a,b,c;
        a=input();
        b=input();
        c=add(&a,&b);
        output(&a,&b,&c);
        return 0;
}