#include<stdio.h>
typedef struct fractions
{
	int num,deno;
	
}fract;
int gcd(int a,int b)
{
	if (b == 0) 
	{
        return a;
    }
	int g=gcd(b, a % b);
	return g;
}

void output(int t,fract result,struct fractions n[])
{
	for(int i=0;i<t-1;i++)
		printf("%d/%d+",n[i].num,n[i].deno);
	printf("%d/%d=%d/%d",n[t-1].num,n[t-1].deno,result.num,result.deno);
}
fract add(fract n,fract sum)
{
	fract result={(n.num * sum.deno) + (n.deno * sum.num), n.deno * sum.deno};
	
	return result;
}
fract reducedfraction(fract n[],int t)
{
	fract sum=n[0];
	int g;
	for(int i=1;i<t;i++)
		sum=add(n[i],sum);
	g=gcd(sum.num,sum.deno);
	sum.num=sum.num/g;
	sum.deno=sum.deno/g;
	return sum;
}
void input_one_fraction(fract *n)
{
	scanf("%d%d",&n->num,&n->deno);
}
void input(fract n[],int *t)
{
	scanf("%d",t);
	for(int i=0;i<*t;i++)
		one_input(&n[i]);
	
}

int main()
{
	fract a,n[100];
	
    int t;
    input(n,&t);
    a=reducedfraction(n,t);
    output(t,a,n);
    return 0;
}
