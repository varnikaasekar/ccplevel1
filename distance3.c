#include<stdio.h>
#include<math.h>
typedef struct point
{
    float x,y;
}Point;

float distance(Point p1,Point p2)
{
    float result;
    result=sqrt((p2.x-p1.x)*(p2.x-p1.x)+(p2.y-p1.y)*(p2.y-p1.y));
	return result;
}
void output(Point p1,Point p2,float a)
{
    printf("The distance between %.2f,%.2f and %.2f,%.2f is %.2f\n",p1.x,p1.y,p2.x,p2.y,a);
	return;
     	
}


void input(Point *p1,Point *p2)
{
	scanf("%f%f",&p1->x,&p1->y);
	scanf("%f%f",&p2->x,&p2->y);
}
int main()
{
    
   Point p1,p2; 
   float a;
   input(&p1,&p2);
   a=distance(p1,p2);
   output(p1,p2,a);
   return 0;
   
}