#include<stdio.h>
void input(int *a,int *b,int *c,int *d)
{
	scanf("%d/%d",a,b);
	scanf("%d/%d",c,d);
}
void add(int a,int b,int c,int d,int *result1,int *result2)
{
	
	*result1=a*d+b*c;
	*result2=d*b;
}
void output(int a,int b,int c,int d,int result1,int result2)	
{
	printf("The sum of %d/%d and %d/%d is %d/%d",a,b,c,d,result1,result2);
}
int main()
{
	int a,b,c,d,result1,result2;
    input(&a,&b,&c,&d);
	add(a,b,c,d,&result1,&result2);
	output(a,b,c,d,result1,result2);
	return 0;
}